//
//  ContentView.swift
//  Memorize
//
//  Created by Miguel Mauricio Sierra on 3/12/20.
//  Copyright © 2020 Learning Miguel Sierra. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello There, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
